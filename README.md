# ScanApp

This software is part of prototype built for the The Open University's Module TM470.

Welcome to the ScanApp hybrid app.

## How to run ScanApp

Please execute the following commands:

```
cordova platform add android
cordova plugin add phonegap-nfc
cordova plugin add cordova-plugin-device
cordova plugin add cordova-plugin-firebase
```

## Firebase private key

The google-service key is not shipped along with the source code of this project.
However, the service is designed to work even is that file is not present. Of course, all the functionalities
concerning the push notifications will not be provided.

If you want to generate a Firebase google-services manifest, please follow the Google Firebase Cloud
Messaging [guide](https://firebase.google.com/docs/admin/setup).

Then place the file `google-services.json` in the `platforms/android`.

## License

The source code is released under the Apache License v2.0

## Author

Alessandro Pieri, 2018