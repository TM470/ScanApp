/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    onDeviceReady: function() {
        var BACKEND_ENDPOINT = "http://10.0.0.13:8080";

        var deviceUUID = null;
        var nfcHandler = onNdef;
        var currentShopId = 1;

        // Private functions

        /**
         * Display error in case the NFC is not enabled.
         */
        function listenerFailed() {
            displayError('Please enable NFC from the phone\'s settings');
        }

        /**
         * Print error on screen.
         * @param message Error message
         */
        function displayError(message) {
            new $.nd2Toast({
                message : message,
                action : {
                    title : "OK",
                    color : "red"
                },
                ttl : 3000
            });
        }

        /**
         * Bind NFC listener to list for 'text/scanapp-product' mime-type
         */
        function bindListener() {
            if (typeof nfc != "undefined") {
                nfc.addTagDiscoveredListener(nfcDispatcher, function() { }, listenerFailed);
                nfc.addMimeTypeListener('text/scanapp-product', nfcDispatcher, function() { }, listenerFailed);
            }
        }

        /**
         * Dispatch call to the bound listener.
         * @param nfcEvent NFC NDEF event
         */
        function nfcDispatcher(nfcEvent) {
            nfcHandler(nfcEvent);
        }

        /**
         * NDEF event listener.
         * @param nfcEvent NDEF records.
         */
        function onNdef(nfcEvent) {
            var tag = nfcEvent.tag;

            // TODO avoid double scan
            //if (currentTagSN == tag.id) {
            //    return;
            //}
            //currentTagSN = tag.id

            var payload = tag.ndefMessage[0]['payload'];
            var payload_str = nfc.bytesToString(payload);
            var product = $.parseJSON(payload_str);

            displayProduct(product.productId, product.shopId);
        }

        /**
         * Fetch and display product.
         * @param productId Product ID
         * @param shopId Shop ID
         */
        function displayProduct(productId, shopId) {
            $.get(BACKEND_ENDPOINT + '/product/' + productId + '?shopId=' + shopId, function(data) {
                currentShopId = shopId;
                $('#productId').val(data.id);
                $('#productImage').attr('src', BACKEND_ENDPOINT + data.photo);
                $('#productBrand').text(data.brand);
                $('#productName').text(data.name);
                $('#productPrice').html('&euro; ' + Number(data.price).toFixed(2));

                $('#checkArray').empty();
                $('#checkArray').controlgroup(); //init

                var sizeHtml = '<legend>Please, select one or more sizes to try-on:</legend>';
                for (var i in data.availableSizes) {
                    var sizeName = data.availableSizes[i].size;
                    var quantity = data.availableSizes[i].quantity;
                    var quantityStr = ' ';
                    if (quantity <= 3) {
                        quantityStr += '&middot; only ' + quantity + ' left in stock';
                    }
                    sizeHtml += '<label for="checkbox-size-' + i + '" style="font-size: 0.9em; align: center;"><input type="checkbox" name="checkbox-size" id="checkbox-size-' + i + '" onchange="app.changeButtonStatus();" value="' + sizeName + '">' + sizeName + ' <span style="font-size: 0.8em; color: #ff0000;">' + quantityStr + '</span></label>';
                }

                $('#checkArray').controlgroup('container').append(sizeHtml);
                $('#checkArray').enhanceWithin().controlgroup('refresh');

                $(':mobile-pagecontainer').pagecontainer('change', '#page2', {reverse: false, changeHash: false});
            }).fail(function() {
                displayError('Error contacting the server. Try again, later.')
            });
        }

        /**
         * Fetch the device UUID and obfuscate it by
         * calculating its hash (SHA256)
         */
        function setDeviceUUID() {
            deviceUUID = sha256(device.uuid);
        }

        /**
         * Display push notification
         */
        function displayNotification(notification) {
            if (notification.tap == false) {
                $('#popupNotificationText').html(notification.body);
                $('#popupNotification').popup('open'); // open popup
            }
        }

        /**
         * Send the firebase token to the backend
         * @param token
         */
        function registerToken(token) {
            $.ajax({
                type: 'PUT',
                url: BACKEND_ENDPOINT + '/device/' + deviceUUID + '?token=' + token,
                contentType: "application/json",
                success: function(data) { },
                dataType: 'json'
            });
        }

        /**
         * Register for Firebase Cloud Messaging events (push notifications)
         */
        function registerFirebase() {
            /* get initial token */
            window.FirebasePlugin.getToken(function(token) {
                registerToken(token);
            }, function(error) {
                alert(error);
            });

            /* add listener to catch a token change */
            window.FirebasePlugin.onTokenRefresh(function(token) {
                registerToken(token);
            }, function(error) {
                alert(error);
            });

            /* add listener for push notifications */
            window.FirebasePlugin.onNotificationOpen(function(notification, data) {
                displayNotification(notification);
            }, function(error) {
                alert(error);
            });
        }

        // Public functions

        /**
         * Disable submit button in case nothing is checked.
         */
        this.changeButtonStatus = function() {
            $('#try-button').prop('disabled', $('#checkArray :checked').length == 0);
        };

        /**
         * Submit a try-on request.
         */
        this.sendTryOn = function() {
            var productId = $('#productId').val();

            var sizes = $("#checkArray :checked").map(function(){
                return $(this).val();
            }).get();

            $.ajax({
                type: 'POST',
                url: BACKEND_ENDPOINT + '/try-on/',
                data: JSON.stringify({"productId": productId, "sizes": sizes, "shopId": currentShopId, "deviceUUID": deviceUUID}),
                contentType: "application/json",
                success: function(data) {
                    $('#returnTryOnId').text(data.id);
                    $(':mobile-pagecontainer').pagecontainer('change', '#page3', {reverse: false, changeHash: false});
                },
                dataType: 'json'
            });
        };

        /**
         * Move back to the main activity deleting history.
         */
        this.goToScan = function() {
            nfcHandler = onNdef;
            $(':mobile-pagecontainer').pagecontainer('change', '#page1', {reverse: false, changeHash: false});
        };

        nfcHandler = onNdef;
        setDeviceUUID();
        registerFirebase();
        bindListener();
        //displayProduct("AO9819-600", "1"); //test from browser
    }
};

app.initialize();